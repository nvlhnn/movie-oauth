const { UserRefreshClient } = require("google-auth-library");
const { Movie, User } = require("../models");
const sendEmail = require("../services/sendEmail.service");
const { Op } = require("sequelize");

class MovieController {
  static findAll = async (req, res, next) => {
    try {
      const data = await Movie.findAll();

      if (data.length > 0) {
        res.status(200).json(data);
      } else {
        res.status(200).json({ message: "Empty Collection" });
      }
    } catch (error) {
      next(error);
    }
  };

  static find = async (req, res, next) => {
    try {
      const data = await Movie.findOne({
        where: { id: req.params.id },
      });

      if (data) {
        res.status(200).json(data);
      } else {
        throw {
          status: 404,
          message: "Resource not found",
        };
      }
    } catch (error) {
      next(error);
    }
  };

  static create = async (req, res, next) => {
    const { name } = req.body;
    const filename = req.file?.filename;
    let poster_url;

    try {
      if (filename) {
        if (process.env.NODE_ENV == "production") {
          poster_url = `${req.hostname}/${filename}`;
        } else {
          poster_url = `localhost:${process.env.PORT}/${filename}`;
        }
      } else {
        poster_url = null;
      }

      const data = await Movie.create({
        name: name,
        poster: poster_url,
      });
      res.status(201).json({ message: "Succesfully add movie" });
    } catch (error) {
      next(error);
    }
  };

  static update = async (req, res, next) => {
    const filename = req.file?.filename;
    let poster_url;

    try {
      if (filename) {
        if (process.env.NODE_ENV == "production") {
          poster_url = `${req.hostname}/${filename}`;
        } else {
          poster_url = `localhost:${process.env.PORT}/${filename}`;
        }

        req.body.poster = poster_url;
      }

      const data = await Movie.update(req.body, {
        where: { id: req.params.id },
        returning: true,
      });

      if (data[1][0]) {
        res.status(200).json({ message: "Successfully update movie" });
      } else {
        throw {
          status: 404,
          message: "Resource not found",
        };
      }
    } catch (error) {
      next(error);
    }
  };

  static destroy = async (req, res, next) => {
    try {
      const data = await Movie.destroy({ where: { id: req.params.id } });

      if (data > 0) {
        res.status(200).json({ message: "Successfully delete movie" });
      } else {
        throw {
          status: 404,
          message: "Resource not found",
        };
      }
    } catch (error) {
      next(error);
    }
  };

  static broadcast = async (req, res, next) => {
    try {
      const { userIds, subject, text } = req.body;

      if (!Array.isArray(userIds) || userIds.length < 1) {
        throw {
          status: 400,
          message: "userIds must be in an array and not empty",
        };
      }

      if (!subject || !text) {
        throw {
          status: 400,
          message: "subject or text required",
        };
      }

      const user = await User.findAll({
        where: {
          id: {
            [Op.in]: userIds,
          },
        },
      });

      const emails = user.map((a) => a.email).toString();

      const html = `
        <pre>
          ${text}
        <pre>
      `;
      await sendEmail("naufalcorp@gmail.com", emails, html, null, subject);

      res.status(200).json({
        message: "Succesfully send broadcast email",
      });
    } catch (error) {
      next(error);
    }
  };
}

module.exports = MovieController;
