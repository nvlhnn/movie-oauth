const { User } = require("../models");
const bcrypt = require("bcryptjs");
const salt = bcrypt.genSaltSync(10);
const jwt = require("jsonwebtoken");
const generateJWT = require("../helper/jwt.helper");
const { OAuth2Client } = require("google-auth-library");
const axios = require("axios");
const client = new OAuth2Client(
  process.env.GOOGLE_CLIENT_ID,
  process.env.GOOGLE_CLIENT_SECRET
);
const sendEmail = require("../services/sendEmail.service");

class AuthController {
  static async login(req, res, next) {
    try {
      if (req.body.email && req.body.password) {
        const user = await User.findOne({
          where: {
            email: req.body.email,
          },
        });

        if (!user) {
          throw {
            status: 401,
            message: "Invalid email or password",
          };
        }
        if (bcrypt.compareSync(req.body.password, user.password)) {
          const token = generateJWT(user);

          res.status(200).json({
            token,
          });
        } else {
          throw {
            status: 401,
            message: "Invalid email or password",
          };
        }
      } else if (req.body.google_id_token) {
        // melakukan verifikasi id token
        const payload = await client.verifyIdToken({
          idToken: req.body.google_id_token,
          requiredAudience: process.env.GOOGLE_CLIENT_ID,
        });
        // mencari email dari google di database
        const user = await User.findOne({
          where: {
            email: payload.payload.email,
          },
        });

        if (user) {
          const token = generateJWT(user);

          res.status(200).json({ token });
        } else {
          const createdUser = await User.create({
            email: payload.payload.email,
          });
          const token = generateJWT(createdUser);

          const html = `
            <pre>
              halo, terimakasih telah mendaftar :)
            <pre>
          `;

          await sendEmail(
            "naufalcorp@gmail.com",
            createdUser.email,
            html,
            null,
            "Registration complete"
          );

          res.status(200).json({ token });
        }
        // mendaftarkan secara otomatis, jika user belom ada di database
      } else if (req.body.facebook_id_token) {
        const response = await axios.get(
          `https://graph.facebook.com/v12.0/me?fields=id%2Cname%2Cemail%2Cgender%2Cbirthday&access_token=${req.body.facebook_id_token}`
        );
        // mencari user di database
        const user = await User.findOne({
          where: {
            email: response.data.email,
            role: 3,
          },
        });

        if (user) {
          const token = generateJWT(user);

          res.status(200).json({ token });
        } else {
          const createdUser = await User.create({
            email: response.data.email,
            role: 3,
          });

          const token = generateJWT(createdUser);

          const html = `
          <pre>
            halo, terimakasih telah mendaftar :)
          <pre>
        `;

          await sendEmail(
            "naufalcorp@gmail.com",
            createdUser.email,
            html,
            null,
            "Registration complete"
          );

          res.status(200).json({ token });
        }
      }
    } catch (err) {
      next(err);
    }
  }

  static register = async (req, res, next) => {
    const { email, password } = req.body;
    const filename = req.file?.filename;
    let photo_profile;

    try {
      if (filename) {
        if (process.env.NODE_ENV == "production") {
          photo_profile = `${req.hostname}/${filename}`;
        } else {
          photo_profile = `localhost:${process.env.PORT}/${filename}`;
        }
      } else {
        photo_profile = null;
      }

      const user = await User.findOne({
        where: {
          email: req.body.email,
        },
      });

      console.log(user);
      if (user) {
        throw {
          status: 400,
          message: "Email already in use",
        };
      }

      await User.create({
        email: email,
        password: bcrypt.hashSync(password, salt),
        photo_profile: photo_profile,
      });

      const html = `
      <pre>
      halo, terimakasih telah mendaftar :)
      <pre>
      `;
      await sendEmail(
        "naufalcorp@gmail.com",
        req.body.email,
        html,
        null,
        "Registration complete"
      );

      res.status(201).json({
        message: "User created",
      });
    } catch (error) {
      next(error);
    }
  };
}

module.exports = AuthController;
