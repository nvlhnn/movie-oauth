const { Wishlist, User, Movie } = require("../models");
// const User = require("../models/user");

class WishlistController {
  static create = async (req, res, next) => {
    // const { username, password } = req.body;

    try {
      const user = await User.findOne({
        where: { email: req.user.email },
      });

      const movie = await Movie.findOne({
        where: { id: req.params.movieId },
      });

      await user.addMovie(movie);
      res.status(201).json(user);
    } catch (error) {
      next(error);
    }
  };

  static getUserWishlists = async (req, res, next) => {
    try {
      const wishlist = await User.findOne({
        where: { id: req.user.id },
        include: Movie,
      });

      res.status(200).json(wishlist.Movies);
    } catch (error) {
      next(error);
    }
  };

  static destroy = async (req, res, next) => {
    // const { username, password } = req.body;

    try {
      const user = await User.findOne({
        where: { email: req.user.email },
      });

      const movie = await Movie.findOne({
        where: { id: req.params.movieId },
      });

      await user.removeMovie(movie);
      res.status(200).json("successfully delete wishlist");
    } catch (error) {
      next(error);
    }
  };
}

module.exports = WishlistController;
