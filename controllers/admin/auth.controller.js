const { User } = require("../../models");
const bcrypt = require("bcryptjs");
const salt = bcrypt.genSaltSync(10);
const jwt = require("jsonwebtoken");
const { OAuth2Client } = require("google-auth-library");
const axios = require("axios");
const generateJWT = require("../../helper/jwt.helper");
const client = new OAuth2Client(
  process.env.GOOGLE_CLIENT_ID,
  process.env.GOOGLE_CLIENT_SECRET
);

class AuthController {
  static async login(req, res, next) {
    try {
      if (req.body.email && req.body.password) {
        const user = await User.findOne({
          where: {
            email: req.body.email,
          },
        });

        if (!user) {
          throw {
            status: 401,
            message: "Invalid email or password",
          };
        }

        if (
          bcrypt.compareSync(req.body.password, user.password) &&
          [1, 2].includes(user.role)
        ) {
          const token = generateJWT(user);

          res.status(200).json({
            token,
          });
        } else {
          throw {
            status: 401,
            message: "Invalid email or password",
          };
        }
      } else {
        throw {
          status: 401,
          message: "Invalid email or password",
        };
      }
    } catch (err) {
      next(err);
    }
  }

  static register = async (req, res, next) => {
    const { email, password } = req.body;
    try {
      const user = await User.findOne({
        where: {
          email: req.body.email,
        },
      });

      if (user) {
        throw {
          status: 400,
          message: "Email already in use",
        };
      }

      await User.create({
        email: email,
        password: bcrypt.hashSync(password, salt),
        role: 2,
      });
      res.status(201).json({
        message: "Admin created",
      });
    } catch (error) {
      next(error);
    }
  };
}

module.exports = AuthController;
