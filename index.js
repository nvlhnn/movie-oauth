const express = require("express");
const errorHandler = require("./middlewares/errorHandler");
const { sequelize, UserGame } = require("./models");
const router = require("./routes");
const app = express();
const port = process.env.PORT;
const morgan = require("morgan");
const Sentry = require("@sentry/node");
const path = require("path");

Sentry.init({
  dsn: process.env.SENTRY_DSN,
});

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(express.static(path.join("public", "src")));
// app.use(express.static("public"));

app.use((req, res, next) => {
  req.sentry = Sentry;
  next();
});

app.use(morgan("tiny"));
require("./config/passport")(app);
app.use(router);

app.use(errorHandler);
app.listen(port, async () => {
  console.log(`Example app listening on port ${port}`);
});
