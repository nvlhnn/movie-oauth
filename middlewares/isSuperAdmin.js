const checkRole = (req, res, next) => {
  try {
    if (req.user.role == 1) {
      return next();
    } else {
      throw {
        status: 403,
        message: "You are not allowed to make this request",
      };
    }
  } catch (error) {}
};

module.exports = checkRole;
