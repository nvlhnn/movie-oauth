function placeSentry(err, req, res, next, s) {
  req.sentry = s;
  next();
}

module.exports = placeSentry;
