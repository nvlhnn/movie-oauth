function errorHandler(err, req, res, next) {
  // console.log(err);

  req.sentry.captureException(err);

  if (err.status) {
    res.status(err.status).json({
      message: err.message,
    });
  } else {
    console.log(err);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
}

module.exports = errorHandler;
