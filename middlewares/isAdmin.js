const checkRole = (req, res, next) => {
  try {
    console.log(req.user.role);
    if ([1, 2].includes(req.user.role)) {
      return next();
    } else {
      throw {
        status: 403,
        message: "You are not allowed to make this request",
      };
    }
  } catch (error) {
    next(error);
  }
};

module.exports = checkRole;
