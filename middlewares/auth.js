// const jwt = require("jsonwebtoken");
// const { User } = require("../models");

// const auth = async (req, res, next) => {
//   try {
//     const token = req.headers.authorization;
//     if (!token) {
//       res.status(401).json({
//         message: "Unauthorized request",
//       });
//     } else {
//       const decoded = jwt.verify(token, "qweqwe");

//       const user = await User.findOne({
//         where: { id: decoded.id },
//         attributes: ["id", "email"],
//       });

//       // const test = await UserGame.findAll();

//       if (user) {
//         req.user = decoded;
//         next();
//       } else {
//         res.status(401).json({
//           message: "Unauthorized request",
//         });
//       }
//     }
//   } catch (error) {
//     next({ status: 401, message: "Unauthorized request" });
//   }
// };

// module.exports = auth;

const passport = require("passport");

const auth = passport.authenticate("jwt", {
  session: false,
} );

module.exports = auth;
