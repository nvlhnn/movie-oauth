const UserController = require("../controllers/user.controller");

const router = require("express").Router();

router.post("/send-forgot-pass-token", UserController.sendForgotPasswordToken);
router.post(
  "/verify-forgot-pass-token",
  UserController.verifyForgotPasswordToken
);
router.post("/change-pass", UserController.changePassword);

module.exports = router;
