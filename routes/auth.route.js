const AuthController = require("../controllers/auth.controller.");

const router = require("express").Router();

const multer = require("multer");
const storage = require("../services/multerStorage.service");
const upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === "image/jpeg" ||
      file.mimetype === "image/jpg" ||
      file.mimetype === "image/png"
    ) {
      cb(null, true);
    } else {
      cb(new Error("File should be an image"), false);
    }
  },
  limits: {
    fileSize: 3000000,
  },
});

router.post("/login", AuthController.login);
router.post(
  "/register",
  [upload.single("photo_profile")],
  AuthController.register
);

module.exports = router;
