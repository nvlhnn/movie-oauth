const Wishlish = require("../controllers/wishlist.controller");
const auth = require("../middlewares/auth");

const router = require("express").Router();

router.post("/:movieId", auth, Wishlish.create);
router.get("/", auth, Wishlish.getUserWishlists);
router.delete("/:movieId", auth, Wishlish.destroy);

module.exports = router;
