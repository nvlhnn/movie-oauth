const router = require("express").Router();

const authRoute = require("./auth.route");
const wishlistRoute = require("./wishlist.route");
const movieRoute = require("./movie.route");
const userRoute = require("./user.route");

const adminRoute = require("./admin.route");

router.use("/auth", authRoute);
router.use("/wishlists", wishlistRoute);
router.use("/movies", movieRoute);
router.use("/users", userRoute);

router.use("/admin", adminRoute);
router.use("/api", router);
// router.get('api/user/stat', (req, res) => res.status(200).json('ok'))
router.get("/api", (req, res) => res.status(404).json("No API route found"));

module.exports = router;
