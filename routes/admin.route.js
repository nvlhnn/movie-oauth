const AuthController = require("../controllers/admin/auth.controller");
const auth = require("../middlewares/auth");
const isSuperAdmin = require("../middlewares/isSuperAdmin");

const router = require("express").Router();

// auth
// router.post("/auth/login", async (req, res) => {
//   console.log("ok");
// });
router.post("/auth/login", AuthController.login);
router.post("/auth/register", [auth, isSuperAdmin], AuthController.register);

module.exports = router;
