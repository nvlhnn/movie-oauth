const Movie = require("../controllers/movie.controller");
const auth = require("../middlewares/auth");
const isAdmin = require("../middlewares/isAdmin");

const router = require("express").Router();

const multer = require("multer");
const storage = require("../services/multerStorage.service");
const upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === "image/jpeg" ||
      file.mimetype === "image/jpg" ||
      file.mimetype === "image/png"
    ) {
      cb(null, true);
    } else {
      cb(new Error("File should be an image"), false);
    }
  },
  limits: {
    fileSize: 3000000,
  },
});

router.get("/", Movie.findAll);
router.get("/:id", Movie.find);
router.post("/", [auth, isAdmin, upload.single("poster")], Movie.create);
router.post("/broadcast", [auth, isAdmin], Movie.broadcast);
router.put("/:id", [auth, isAdmin, upload.single("poster")], Movie.update);
router.delete("/:id", [auth, isAdmin], Movie.destroy);

module.exports = router;
